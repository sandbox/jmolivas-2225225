<?php
/*
  Do not include drupal's default style sheet in this theme !
*/
function console_stylesheet_import($stylesheet, $media = 'all') {
  if (strpos($stylesheet, 'misc/drupal.css') == 0) {
    return theme_stylesheet_import($stylesheet, $media);
  }
}

function console_book_navigation($node) {
    
  $output = '';
  $links = '';

  if ($node->nid) {
    $tree = book_tree($node->nid);

    if ($prev = book_prev($node)) {
      drupal_add_link(array('rel' => 'prev', 'href' => url('node/'. $prev->nid)));
      $links .= l(t('‹ ') . $prev->title, 'node/'. $prev->nid, array('class' => 'page-previous', 'title' => t('Go to previous page')));
    }
    if ($node->parent) {
      drupal_add_link(array('rel' => 'up', 'href' => url('node/'. $node->parent)));
      $links .= l(t('up'), 'node/'. $node->parent, array('class' => 'page-up', 'title' => t('Go to parent page')));
    }
    if ($next = book_next($node)) {
      drupal_add_link(array('rel' => 'next', 'href' => url('node/'. $next->nid)));
      $links .= l($next->title . t(' ›'), 'node/'. $next->nid, array('class' => 'page-next', 'title' => t('Go to next page')));
    }

    if (isset($tree) || isset($links)) {
       
      $output .= '<div class="book-navigation">';
      if (isset($tree)) {
      $output .= '---------------------<span style="color:#00EE00">----------</span><span style="color:#00DD00">--------</span><span style="color:#00CC00">-----</span><span style="color:#00BB00">---</span><span style="color:#00AA00">---</span><span style="color:#009900">--</span><span style="color:#008800">--</span><span style="color:#007700">--</span><span style="color:#006600">-</span><span style="color:#005500">--</span><span style="color:#004400">--</span><span style="color:#003300">--</span><span style="color:#002200">-</span>';
        $output .= $tree;
      }
      if (isset($links)) {
        $output .= '---------------------<span style="color:#00EE00">----------</span><span style="color:#00DD00">--------</span><span style="color:#00CC00">-----</span><span style="color:#00BB00">---</span><span style="color:#00AA00">---</span><span style="color:#009900">--</span><span style="color:#008800">--</span><span style="color:#007700">--</span><span style="color:#006600">-</span><span style="color:#005500">--</span><span style="color:#004400">--</span><span style="color:#003300">--</span><span style="color:#002200">-</span>';
      }
      if (isset($links)) { 
        $output .= '<div class="page-links">'. $links .'</div>';
      }
      $output .= '</div>';
      if (isset($links)) {
         $output .= '---------------------<span style="color:#00EE00">----------</span><span style="color:#00DD00">--------</span><span style="color:#00CC00">-----</span><span style="color:#00BB00">---</span><span style="color:#00AA00">---</span><span style="color:#009900">--</span><span style="color:#008800">--</span><span style="color:#007700">--</span><span style="color:#006600">-</span><span style="color:#005500">--</span><span style="color:#004400">--</span><span style="color:#003300">--</span><span style="color:#002200">-</span>';
      } 
    }
  }

  return $output; 
}
  

?>
