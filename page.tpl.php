<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<span style="color:#00FF00">================================================</span><span style="color:#00EE00">====</span><span style="color:#00DD00">====</span><span style="color:#00CC00">====</span><span style="color:#00BB00">====</span><span style="color:#00AA00">====</span><span style="color:#009900">====</span><span style="color:#008800">====</span><span style="color:#007700">====</span><span style="color:#006600">===</span><span style="color:#005500">===</span><span style="color:#004400">===</span><span style="color:#003300">===</span>

<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td id="logo">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </td>
    <td id="menu">
      <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
      <?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>
      <?php print $search_box ?>
    </td>
  </tr>
  <tr>
    <td colspan="2"><div><?php print $header ?></div></td>
  </tr>
</table>

<span style="color:#00FF00">================================================</span><span style="color:#00EE00">====</span><span style="color:#00DD00">====</span><span style="color:#00CC00">====</span><span style="color:#00BB00">====</span><span style="color:#00AA00">====</span><span style="color:#009900">====</span><span style="color:#008800">====</span><span style="color:#007700">====</span><span style="color:#006600">===</span><span style="color:#005500">===</span><span style="color:#004400">===</span><span style="color:#003300">===</span>


<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>
        <?php if ($breadcrumb) {?>---------------------<span style="color:#00EE00">----------</span><span style="color:#00DD00">--------</span><span style="color:#00CC00">-----</span><span style="color:#00BB00">---</span><span style="color:#00AA00">---</span><span style="color:#009900">--</span><span style="color:#008800">--</span><span style="color:#007700">--</span><span style="color:#006600">-</span><span style="color:#005500">--</span><span style="color:#004400">--</span><span style="color:#003300">--</span><span style="color:#002200">-</span><?php } ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<?php if ($footer_message) { ?>
<span style="color:#00FF00">================================================</span><span style="color:#00EE00">====</span><span style="color:#00DD00">====</span><span style="color:#00CC00">====</span><span style="color:#00BB00">====</span><span style="color:#00AA00">====</span><span style="color:#009900">====</span><span style="color:#008800">====</span><span style="color:#007700">====</span><span style="color:#006600">===</span><span style="color:#005500">===</span><span style="color:#004400">===</span><span style="color:#003300">===</span>
<?php } ?>

<div id="footer">
  <?php print $footer_message ?>
</div>
<?php print $closure ?>
</body>
</html>
